# Demo App

## App image

* build

    `docker build -t registry.gitlab.com/nstetter/demo-app -f ./srv/Dockerfile ./srv`

* push

    `docker push registry.gitlab.com/nstetter/demo-app:latest`

## Deployment

### scale

* imperative:

    `kubectl scale deployment --replicas=5 demo-deployment`

* declarative:

    change number of replicas in `demo-deployment.yaml` and apply: `kubectl apply -f demo-deployment.yaml`

### update image

* trigger pulling of newer images:

    `kubectl rollout restart deployment demo-deployment`

* watch update happen:

    `kubectl get deployment`

    `kubectl get pods`

    `while :; do curl -s "$(minikube ip)"/index | grep -oP "(?<=<h1>)Version.*(?=</h1>)"; done`
    
### kill pods

`kubectl delete pod demo-deployment-xxxx`

watch number of pods:

`kubectl get deployment`

## GKE setup

kubernetes version needs to be >= 1.19

The ingress needs to be of class nginx, which does not come with GKE by default.
Install with helm:

```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install my-release ingress-nginx/ingress-nginx
```

## Sources

* [Sander Knape](https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/)
* [Gitlab](https://about.gitlab.com/blog/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/)
* [Kartaca](https://kartaca.com/en/deploy-your-application-directly-from-gitlab-ci-to-google-kubernetes-enginegke/)
